import { Component, OnInit } from '@angular/core';
import { Instructor} from './assign-instructor';
import {AssignInstructorService} from './assign-instructor.service';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
import {offeringInstructor} from './offeringInstructor';
import { TokenService} from '../services/token.service';
import { NotifierService } from 'angular-notifier';
@Component({
  selector: 'app-assign-instructor',
  templateUrl: './assign-instructor.component.html',
  styleUrls: ['./assign-instructor.component.css']
})
export class AssignInstructorComponent implements OnInit {
  instructors:Instructor[];
  offering: offeringInstructor;
  selectedCode:number;
  departmentId=null;
  listButton=null;
  message=null
  public offer={
    instructor:null,
    offering:null

  };
  total=0;
  page=1;
  pageSize=10
  expanded=[];
  courseCode=null;
  constructor(private instructorService: AssignInstructorService,private token: TokenService,private router:Router,private notification: NotifierService,private route:ActivatedRoute) { }

  
  ngOnInit() {
    this.departmentId=this.token.getUser();
    this.route.paramMap.subscribe(
      params=>{
       this.selectedCode=+params.get('id');
       console.log(this.selectedCode)

      }
    );
   this.instructorService.getInstructor(this.departmentId).subscribe(
      instructors=>this.handleGetResponse(instructors)
    );
  }
  handleGetResponse(instructors)
  {
    this.instructors=instructors;
    for(var i:number=0;i<this.instructors.length;i++)
    {
      this.expanded[i]=false;

    }

  }
  pageChanged(event){
    this.page = event;
  }
  assignInstructor(id:number):void{
    
    this.instructorService.assignInstructor(id,this.selectedCode,this.departmentId).subscribe(offer=>this.handleRespone(offer));
  }
  handleRespone(offer)
  {
    console.log(offer);
      console.log("hello");
      console.log(offer.courseCode);
      // this.message="show";
      console.log(offer.offering);
    if(offer.message === "Succesfull")
    {
      
      this.offering=offer.offering;
      this.courseCode=offer.courseCode;
      console.log("inside message succesful");
     
      this.notification.notify("success", "Instructor with id "+" "+this.offering.instructor+" "+" has been assigned succesfully");
    }
    else if(offer.error){
      console.log("under error");
       console.log(offer.error);
      this.notification.notify("warning", offer.error);
      
    }
    

  }
  listButtons(){
    console.log("Actions Button  clicked");
    if(!this.listButton)
    {
        console.log("listButton is null");
        this.listButton="show";
        
    }
    else{
      this.listButton=null;
    }
  }
  toggleFloat(i:number)
  {
    this.expanded[i]=!this.expanded[i];
  }



}
