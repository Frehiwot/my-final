export class Instructor{
    id: number;
   userId: string;
   firstName: string;
   middleName:string;
   lastName:string;
   department:string;
   phone: number;
   email: string;
   role: string;
   password: string;
}