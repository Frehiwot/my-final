import { TestBed } from '@angular/core/testing';

import { AssignInstructorService } from './assign-instructor.service';

describe('AssignInstructorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssignInstructorService = TestBed.get(AssignInstructorService);
    expect(service).toBeTruthy();
  });
});
