import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Instructor} from './assign-instructor';
import { offeringInstructor } from './offeringInstructor';
@Injectable({
  providedIn: 'root'
})
export class AssignInstructorService {
  public offer={
    instructor:null,
    offering:null

  };
  constructor(private httpClient: HttpClient) { }

  getInstructor(departmentHeadId):Observable<Instructor[]>{
    return this.httpClient.get<Instructor[]>(`http://localhost:8000/departmentHead/${departmentHeadId}/listInstructors`);
  }
  assignInstructor(instructor:number,offering:number,departmentHeadId):Observable<number>{
    this.offer.instructor=instructor;
    this.offer.offering=offering;
    console.log(this.offer);
    return this.httpClient.post<number>(`http://localhost:8000/departmentHead/${departmentHeadId}/storeOfferingInstructors`,this.offer);
  }
}
