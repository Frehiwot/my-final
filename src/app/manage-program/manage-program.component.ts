import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.all.min.js'
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-manage-program',
  templateUrl: './manage-program.component.html',
  styleUrls: ['./manage-program.component.css']
})
export class ManageProgramComponent implements OnInit {
    public form={
        programTitle:null,
        programDuration:null,
        studyLanguage:null,
        min_overall_credit:null,
        max_overall_credit:null,
        minimum_CGPA:null,
        creditHours:null,
        ects:null,
        credit:null
      };
      departmentId=null
      programs=[]
      addButton=null;
  
      expanded=[];
      total=0;
      page=1;
      pageSize=5
  constructor(private httpClient:HttpClient,private Token: TokenService,private router: Router,private notification: NotifierService) { }

  ngOnInit() {
    this.departmentId=this.Token.getUser();
    console.log(this.departmentId);
    this.loadPrograms();
    this.total=this.programs.length;
  
  }
  loadPrograms()
  {
    this.httpClient.get<[]>(`http://localhost:8000/departmentHead/${this.departmentId}/programs`).subscribe(programs=>
    {
        this.programs=programs;
        console.log(programs);
        console.log(this.programs);
        for(var i:number=0;i<this.programs.length;i++)
        {
            this.expanded[i]=false;
    
        }
    }
);
  }
  pageChanged(event){
    this.page = event;
  }
  onSubmit(){
    console.log(this.form);
    let responseProgram=null;
    this.httpClient.post(`http://localhost:8000/departmentHead/${this.departmentId}/register/program`,this.form).subscribe(program=>
        {
          console.log(program);
          responseProgram=program;
          if(responseProgram.message === "succesfull")
          {
            this.notification.notify("success", " You have saved succesfully"); 
            this.programs.push(program)
          }
          else if(responseProgram.error){
            this.notification.notify("warning", responseProgram.error); 
          }
        
        
        }
     );
 }
 show(){
    console.log("add button clicked");
    if(!this.addButton)
    {
        console.log("addButton is null");
        this.addButton="show";
        
    }
    else{
      this.addButton=null;
    }
  }
  delete(program){
    let responsee=null;
    Swal.fire({
      confirmButtonColor: '#2A66A7',
      title: "Are u sure u want to delete",


      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: "Ok",
      cancelButtonText: "cancel"
    }).then((result) => {
      if (result.value) {
       this.httpClient.request('delete',`http://localhost:8000/departmentHead/${this.departmentId}/program/${program.programTitle}/delete`).subscribe((response)=>{
          console.log(response);
          console.log("under deleting");
          console.log(response);
          responsee=response;
          if(responsee.message)
          {
            this.notification.notify("success", " Deletion Succesfully"); 
            this.loadPrograms();
           
          }
          else if(responsee.error)
          {
            this.notification.notify("warning", responsee.error);
          }
          
        });

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })
    
  }
 
  toggleFloat(i:number)
  {
    console.log("under toggle");
    console.log(this.expanded[i]);
    this.expanded[i]=!this.expanded[i];
    console.log(this.expanded[i]);
  }

}