import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../../services/token.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-edit-program',
  templateUrl: './edit-program.component.html',
  styleUrls: ['./edit-program.component.css']
})
export class EditProgramComponent implements OnInit {
    public form={
        programTitle:null,
        programDuration:null,
        studyLanguage:null,
        min_overall_credit:null,
        max_overall_credit:null,
        minimum_CGPA:null,
        creditHours:null,
        ects:null,
        credit:null
      };
      departmentId=null
      programs=[]
      addButton=null;
      program=null
      expanded=[];
      selectedProgram=null;

      constructor(private httpClient:HttpClient,private Token: TokenService,private route: ActivatedRoute,private router: Router,private notification: NotifierService) { }

  ngOnInit() {
    this.departmentId=this.Token.getUser();
    console.log(this.departmentId);
    this.route.paramMap.subscribe(
        params=>{
          this.selectedProgram=params.get('title');
         console.log( this.selectedProgram);
  
        }
      );
      this.loadProgram();
  }
  loadProgram()
  {
    this.httpClient.get(`http://localhost:8000/departmentHead/${this.departmentId}/program/${this.selectedProgram}`).subscribe(program=>
    {
        this.program=program;
        console.log(program);
        console.log(this.program);
        this.form={
            programTitle:this.program.programTitle,
            programDuration:this.program.programDuration,
            studyLanguage:this.program.studyLanguage,
            min_overall_credit:this.program.min_overall_credit,
            max_overall_credit:this.program.max_overall_credit,
            minimum_CGPA:this.program.minimum_CGPA,
            creditHours:this.program.creditHours,
            ects:this.program.ECTS,
            credit:this.program.credit
          };
       
    });
  }
  onSubmit(){
    let programResponse=null;
    console.log(this.form);
    this.httpClient.put(`http://localhost:8000/departmentHead/${this.departmentId}/program/${this.selectedProgram}/update`,this.form).subscribe(program=>
        {
        console.log(program);
        programResponse=program;
        if(programResponse.message)
        {
          this.notification.notify("success", " You have updated Succesfully"); 
        }
        else if(programResponse.error)
        {
          this.notification.notify("warning", programResponse.error); 

        }
        // this.programs.push(program)
        }
     );
 }

}