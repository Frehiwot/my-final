import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TokenService } from '../services/token.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public loggedIn :boolean;
  userType=null;
  userId=null
  constructor(private Auth: AuthService,private router:Router,private route: ActivatedRoute,private Token: TokenService) { }

  ngOnInit() {
    this.Auth.authStatus.subscribe( value => this.loggedIn = value);
    this.userType=this.Token.getUserType();
    this.userId=this.Token.getUser();
    console.log(this.userType);
    
    // 
    // this.route
  }
  
  logout(event: MouseEvent){
    console.log("in log out function");
    event.preventDefault();
    this.Token.remove();
    this.Auth.changeAuthStatus(false);
    
    this.router.navigateByUrl('');
    
  
    }


}
