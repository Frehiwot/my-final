import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-chat-bot',
  templateUrl: './chat-bot.component.html',
  styleUrls: ['./chat-bot.component.css']
})
export class ChatBotComponent implements OnInit {

  constructor(private httpClient: HttpClient,private router:Router,private route:ActivatedRoute) { }
 selectedStudent=null;
 Question='';
 datas=[];
 response=null;

  ngOnInit() {
   let comment="my self \n is ur self";
   console.log(comment);

    this.route.paramMap.subscribe(
      params=>{
       this.selectedStudent=+params.get('id');
       console.log(this.selectedStudent)

      }
    );
    let combined={"role":"prolog","display":"Hello,i am your advisor.What can i help u"};
    let combined2={"role":"user","display":comment};
    this.datas.push(combined);
    // this.datas.push(combined2);
    
     
  }
  onSubmit()
  {
    let combined={"role":"user","display":this.Question};
    this.datas.push(combined);
    
    console.log("question is"+this.Question);
    console.log(this.datas);
    console.log(this.datas[0].display);

    this.httpClient.get(`http://localhost:7900/advisor/expert/system/?question=${this.Question}&studentid=3`).subscribe((response)=>{
      console.log("inside response");
      this.settingTimeout(response);
      // this.datas.push(response);
      // console.log(response);
 
     });
    
    console.log("answer is"+this.datas);

  }
  settingTimeout(response)
  {
    console.log(this.datas);
    setTimeout(()=>
    {
      this.datas.push(response)
    },1000);
  }

}