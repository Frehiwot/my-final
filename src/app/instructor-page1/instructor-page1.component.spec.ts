import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorPage1Component } from './instructor-page1.component';

describe('InstructorPage1Component', () => {
  let component: InstructorPage1Component;
  let fixture: ComponentFixture<InstructorPage1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorPage1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorPage1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
