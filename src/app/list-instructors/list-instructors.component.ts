import { Component, OnInit } from '@angular/core';
import {AssignInstructorService} from '../assign-instructor/assign-instructor.service';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
import { Instructor} from '../assign-instructor/assign-instructor';
import { TokenService } from '../services/token.service';
@Component({
  selector: 'app-list-instructors',
  templateUrl: './list-instructors.component.html',
  styleUrls: ['./list-instructors.component.css']
})
export class ListInstructorsComponent implements OnInit {
  instructors:Instructor[];
  departmentId=null;
  listButton=null;
  expanded=[]
  total=0;
 page=1;
 pageSize=5
  constructor(private instructorService: AssignInstructorService,private token: TokenService) { }

  ngOnInit() {
    this.departmentId=this.token.getUser();
    this.loadInstructors();

    console.log(this.instructors);
    this.total=this.instructors.length;
  }
  loadInstructors()
  {
    this.instructorService.getInstructor(this.departmentId).subscribe(
      instructors=>this.instructors=instructors
    );
  }
  pageChanged(event){
    this.page = event;
  }
  listButtons(){
    console.log("Actions Button  clicked");
    if(!this.listButton)
    {
        console.log("listButton is null");
        this.listButton="show";
        
    }
    else{
      this.listButton=null;
    }
  }
  toggleFloat(i:number)
  {
    this.expanded[i]=!this.expanded[i];
  }

}
