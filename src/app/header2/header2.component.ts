import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TokenService } from '../services/token.service';
import {NotificationService} from '../notification/notification.service';
import '../../../node_modules/jquery/dist/jquery.js';
import '../../../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js';
@Component({
  selector: 'app-header2',
  templateUrl: './header2.component.html',
  styleUrls: ['./header2.component.css']
})
export class Header2Component implements OnInit {
  public loggedIn :boolean;
  userType=null;
  expand=false
  userId=null;
  notifications=[]
  notificationCount=0;
  collapses=false;
  constructor(private notificationService:NotificationService,private Auth: AuthService,private router:Router,private route: ActivatedRoute,private Token: TokenService) { }

  ngOnInit() {
    this.Auth.authStatus.subscribe( value => this.loggedIn = value);
    this.userType=this.Token.getUserType();
    this.userId=this.Token.getUser();
    console.log(this.userType);
    this.notificationService.getNotifications(this.userId).subscribe(
      notification=>this.handleNotifications(notification)
      
    );
    
    // 
    // this.route
  }
  handleNotifications(notification)
  {
    console.log(notification);
    if(notification)
    {
       console.log(notification[0].data.message);

    }
   
    this.notifications=notification
    this.notificationCount=this.notifications.length;
  }
  collapse()
  {
    this.collapse!=this.collapse
  }
  toggle()
  {
    console.log("clicked");
    console.log(this.expand);
    this.expand=!this.expand;
  }
  
  logout(event: MouseEvent){
    console.log("in log out function");
    event.preventDefault();
    this.Token.remove();
    this.Auth.changeAuthStatus(false);
    
    this.router.navigateByUrl('');
    
  
    }

}
