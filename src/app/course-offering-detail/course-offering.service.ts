import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { offerCourse} from './course-offering';
import { offeringInstructor } from './offering-instructor';
import { Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CourseOfferingService {

  constructor(private httpClient: HttpClient) { }
  getOffering(instructorId):Observable<offerCourse[]>{
    return this.httpClient.get<offerCourse[]>(`http://localhost:8000/departmentHead/instructorOffering/${instructorId}`);
  }
  deleteOffering(instrucId,id):Observable<{}>{
    return this.httpClient.request('delete',`http://localhost:8000/departmentHead/instructor/offering/${id}/delete`);
  }

}
