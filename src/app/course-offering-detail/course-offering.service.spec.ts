import { TestBed } from '@angular/core/testing';

import { CourseOfferingService } from './course-offering.service';

describe('CourseOfferingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CourseOfferingService = TestBed.get(CourseOfferingService);
    expect(service).toBeTruthy();
  });
});
