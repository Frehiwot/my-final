import { Component, OnInit } from '@angular/core';
import { CourseOfferingService} from './course-offering.service';
import { offerCourse} from './course-offering';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.all.min.js'
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-course-offering-detail',
  templateUrl: './course-offering-detail.component.html',
  styleUrls: ['./course-offering-detail.component.css']
})
export class CourseOfferingDetailComponent implements OnInit {
  courses: offerCourse[]
  selectedInstructor:number
  constructor(private offeringService: CourseOfferingService,private router:Router,private route:ActivatedRoute,private notification: NotifierService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params=>{
       this.selectedInstructor=+params.get('id');
       console.log(this.selectedInstructor)

      }
    );
    this.loadOfferings();
  

  }
  loadOfferings()
  {
    this.offeringService.getOffering(this.selectedInstructor).subscribe(
      course=>{
        console.log(course);
        this.courses=course
      }

    );
  }
  delete(course)
  {
    console.log("under deleting");
    console.log(course);
    let responsee=null;
    
    let deletionResponse=null;
    Swal.fire({
      confirmButtonColor: '#2A66A7',
      title: "Are you sure you want to delete this offering",
      text:"Deleting this course will make this course offering unassigned",


      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: "Ok",
      cancelButtonText: "cancel"
    }).then((result) => {
      if (result.value) {

        this.offeringService.deleteOffering(this.selectedInstructor,course.id).subscribe((response)=>{
          console.log(response);
          responsee=response;
          if(responsee.message)
          {
            this.notification.notify("success", " Deletion Succesfully"); 
            this.loadOfferings();
           
          }
          else if(responsee.error)
          {
            this.notification.notify("warning", responsee.error);
          }
          
        })
        
          
      

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })

  }

}
