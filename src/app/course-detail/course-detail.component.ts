import { Component, OnInit } from '@angular/core';
import {CourseDetailService} from './course-detail.service';
import { Course } from './course';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {

  courses: Course[]
  selectedCode:number
  constructor(private courseService: CourseDetailService,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params=>{
       this.selectedCode=+params.get('id');
       console.log(this.selectedCode)

      }
    );
    this.courseService.getCourse(this.selectedCode).subscribe(
      course=>this.courses=course
    );
  }

}
