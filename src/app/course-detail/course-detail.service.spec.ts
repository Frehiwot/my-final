import { TestBed } from '@angular/core/testing';

import { CourseDetailService } from './course-detail.service';

describe('CourseDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CourseDetailService = TestBed.get(CourseDetailService);
    expect(service).toBeTruthy();
  });
});
