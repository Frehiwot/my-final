import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course } from './course';
@Injectable({
  providedIn: 'root'
})
export class CourseDetailService {

  constructor(private httpClient: HttpClient) { }
  getCourse(courseId:number):Observable<Course[]>{
    return this.httpClient.get<Course[]>(`http://localhost:8000/course/${courseId}`);
  }
}
