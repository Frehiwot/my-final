import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public form={
    email:null,password:null
  };
  title = 'ngrx';
  showSpinner = false;
  public error=null;
  userType=null
  constructor(private http: HttpClient,private Token: TokenService,private route: Router,private auth: AuthService) { }

  ngOnInit() {
    if(this.Token.loggedIn)
    {
      this.userType=this.Token.getUserType();
      if(this.userType === "Admin")
      {
        
        this.route.navigateByUrl('/admin/users');
      }
      else if(this.userType=== "Instructor")
      {
        this.route.navigateByUrl(`/instructor/${this.Token.getUser()}/courses`);
      }
      else if(this.userType === "Student")
      {
        this.route.navigateByUrl(`/Student/${this.Token.getUser()}/registered/courses`);
       
      }
      else if(this.userType === "Registral Clerk")
      {
        this.route.navigateByUrl('/registralClerk/students');
      
      }
      else if(this.userType === "Department Head")
      {
        this.route.navigateByUrl('/departmentHead/courses');
        
      }
    }
  }
  onSubmit(){
    // title = 'ngrx';
    this.showSpinner = true;
    console.log('under submit');
     return this.http.post('http://localhost:8000/api/login',this.form).subscribe(
       data=> {
         console.log('under data');
        
         this.handleResponse(data);


       },
       error=> this.handleError(error)
     );
  }
  handleResponse(data){
    this.showSpinner=false;
    this.Token.handle(data.access_token,data.userId,data.userType);
    this.auth.changeAuthStatus(true);

    // this.route.navigateByUrl('/infor');
    console.log(data);
    if(data.userType === "Admin")
    {
      this.route.navigateByUrl('/admin/users');
    }
    else if(data.userType === "Instructor")
    {
      this.route.navigateByUrl(`/instructor/${data.userId}/courses`);
    }
    else if(data.userType === "Student")
    {
      // this.route.navigateByUrl(`/student/${data.userId}/course`,{skipLocationChange: false}).then(()=>{
      //   this.route.navigate([`/student/${data.userId}/course`])
      // })
      this.route.navigateByUrl(`/Student/${data.userId}/registered/courses`);
    }
    else if(data.userType === "Registral Clerk")
    {
      this.route.navigateByUrl('/registralClerk/students');
     
    }
    else if(data.userType === "Department Head")
    {
      this.route.navigateByUrl('/departmentHead/courses');
      
    }
  }

  handleError(error){
    this.error=error.error.error;

  }

}
