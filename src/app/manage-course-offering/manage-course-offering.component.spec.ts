import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCourseOfferingComponent } from './manage-course-offering.component';

describe('ManageCourseOfferingComponent', () => {
  let component: ManageCourseOfferingComponent;
  let fixture: ComponentFixture<ManageCourseOfferingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCourseOfferingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCourseOfferingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
