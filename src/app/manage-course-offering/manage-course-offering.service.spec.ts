import { TestBed } from '@angular/core/testing';

import { ManageCourseOfferingService } from './manage-course-offering.service';

describe('ManageCourseOfferingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageCourseOfferingService = TestBed.get(ManageCourseOfferingService);
    expect(service).toBeTruthy();
  });
});
