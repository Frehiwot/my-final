export class CourseOffering{
    id:number;
    course:string;
    semester:number;
    year:Date;
}