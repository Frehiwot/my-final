import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {CourseOffering} from './manage-course-offering';
@Injectable({
  providedIn: 'root'
})
export class ManageCourseOfferingService {

  constructor(private httpClient: HttpClient) { }
  getCourseOffering(departmentHeadId):Observable<CourseOffering[]>{
    return this.httpClient.get<CourseOffering[]>(`http://localhost:8000/departmentHead/${departmentHeadId}/listOffering`);
  }
  addCourseOffering(course,departmentId):Observable<CourseOffering>{
    return this.httpClient.post<CourseOffering>(`http://localhost:8000/departmentHead/${departmentId}/storeOffering`,course);
   
  }
  deleteOffering(departmentId,id):Observable<{}>{
    return this.httpClient.request('delete',`http://localhost:8000/departmentHead/${departmentId}/offering/${id}/delete`);
  }
  // updateCourse(course: ManageCourse):Observable<ManageCourse>{
  //   return this.httpClient.put<ManageCourse>(`http://localhost:8000/updateCourse/${course.id}`,course);
  // }

}
