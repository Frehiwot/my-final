import { Component, OnInit } from '@angular/core';
import {ManageCourseOfferingService} from './manage-course-offering.service';
import {CourseOffering} from './manage-course-offering';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
import { TokenService } from '../services/token.service';
import { SemesterService} from '../semester/semester.service';
import {Semester} from '../semester/semester';
import { NotifierService } from 'angular-notifier';
import Swal from 'sweetalert2/dist/sweetalert2.all.min.js'


@Component({
  selector: 'app-manage-course-offering',
  templateUrl: './manage-course-offering.component.html',
  styleUrls: ['./manage-course-offering.component.css']
})
export class ManageCourseOfferingComponent implements OnInit {
  offering: CourseOffering[]
  selectedCode:string;
  addButton=null;
  listButton=null;
  departmentId=null;
  semesters:Semester[]
  offeringForm={
    course:null,
    semester:null,
    year:null,
    acadamicYear:null,
  }
  expanded=[];
  total=0;
  page=1;
  pageSize=5
  constructor(private offeringService: ManageCourseOfferingService,private semesterService: SemesterService,private notification: NotifierService,private Token: TokenService,private router:Router,private route:ActivatedRoute) { }
  
  ngOnInit() {
    this.addButton=null;
    this.route.paramMap.subscribe(
      params=>{
       this.selectedCode=params.get('id');
       this.offeringForm.course=this.selectedCode;
       console.log(this.selectedCode)

      }
    );
    console.log(this.selectedCode);
    this.departmentId=this.Token.getUser();
    console.log(this.departmentId);
    this.loadOfferings();
    this.loadSemsters();
    console.log("parsing josn");
    console.log(JSON.parse('{"semester": 1}'));
//     var string = '{ "name":"John", "age":30, "city":"New York"}';
// var obj = JSON.parse(string);
    this.total=this.offering.length;
    
   

    
  }
  pageChanged(event){
    this.page = event;
  }
  loadOfferings()
  {
    this.offeringService.getCourseOffering(this.departmentId).subscribe(
      offering=>this.handleGetResponse(offering)
    );
  }
  loadSemsters()
  {
    console.log("under semesters");
    this.semesterService.getSemesters(this.departmentId).subscribe(
      Semester=>{
        console.log("consoling semester");
        console.log(Semester);
        this.semesters=Semester
      }
    );

  }
  handleGetResponse(offering)
  {
    console.log(offering);
    this.offering=offering;
    for(var i:number=0;i<this.offering.length;i++)
      {
        this.expanded[i]=false;
  
      }
  }
  onSubmit()
  {
    this.offeringService.addCourseOffering(this.offeringForm,this.departmentId).subscribe(
      offering=>this.handlePostResponse(offering)
    );
  }
  add():void{
    console.log(this.offeringForm);
    let semester=JSON.parse(this.offeringForm.semester);
    this.offeringForm.semester=semester.semester;
    this.offeringForm.acadamicYear=semester.AcadamicYear;
    console.log("offeringForm");
    console.log(this.offeringForm);
    console.log(semester.semester);
    console.log(this.offeringForm.semester);
    // console.log(offering.semester);
    this.offeringService.addCourseOffering(this.offeringForm,this.departmentId).subscribe(
      offering=>this.handlePostResponse(offering)
    );
  }
  handlePostResponse(offering)
  {
    console.log(offering);
   
    if(offering.message === "succesfull")
    {
      this.notification.notify("success", " You have saved succesfully"); 
      this.offering.push(offering.offering);
    }
    else if(offering.error){
      this.notification.notify("warning", offering.error); 
    }

  }
  show(){
    console.log("add button clicked");
    if(!this.addButton)
    {
        console.log("addButton is null");
        this.addButton="show";
        
    }
    else{
      this.addButton=null;
    }
  }
  listButtons(){
    console.log("Actions Button  clicked");
    if(!this.listButton)
    {
        console.log("listButton is null");
        this.listButton="show";
        
    }
    else{
      this.listButton=null;
    }
  }
  delete(course)
  {
    console.log("under deleting");
    console.log(course);
    let responsee=null;
    
    let deletionResponse=null;
    Swal.fire({
      confirmButtonColor: '#2A66A7',
      title: "Are you sure you want to delete this offering",
      text:"Deleting this course will make course"+" "+ course.course + " "+"unoffered",


      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: "Ok",
      cancelButtonText: "cancel"
    }).then((result) => {
      if (result.value) {
        this.offeringService.deleteOffering(this.departmentId,course.id).subscribe((response)=>{
          console.log(response);
          responsee=response;
          if(responsee.message)
          {
            this.notification.notify("success", " Deletion Succesfully"); 
            this.loadOfferings();
            this.loadSemsters();
           
          }
          else if(responsee.error)
          {
            this.notification.notify("warning", responsee.error);
          }
          
        })

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })
  }
  toggleFloat(i:number)
  {
    this.expanded[i]=!this.expanded[i];
  }
}
