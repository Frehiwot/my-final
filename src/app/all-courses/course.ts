export class course{
    code:string;
    title:string;
    lectureHours:number;
    laboratoryHours:number;
    creditHours:number;
    ECTS:number;
    offered:number;
    requisite:string;
    module:string;
    semester:number;
    result:number;
}