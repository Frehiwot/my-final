import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute,Router} from '@angular/router';
import {Observable} from 'rxjs';
import {course} from './course';
import { Route } from '@angular/compiler/src/core';
@Component({
  selector: 'app-all-courses',
  templateUrl: './all-courses.component.html',
  styleUrls: ['./all-courses.component.css']
})
export class AllCoursesComponent implements OnInit {
  courses:course[]
  selectedStudent:number
  total=0;
 page=1;
 pageSize=5
  constructor(private http:HttpClient,private route:ActivatedRoute,private router:Router) { }
  
  ngOnInit() {
    this.route.paramMap.subscribe(
      params=>{
       this.selectedStudent=+params.get('id');
       console.log(this.selectedStudent)

      }
    );
    this.loadCourses();
    this.total=this.courses.length;
    
  }
  loadCourses()
  {
    this.http.get<course[]>(`http://localhost:8000/courses/registered/${this.selectedStudent}`).subscribe(
      courses=>this.courses=courses
    );

  }
  pageChanged(event){
    this.page = event;
  }

}

