import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { TokenService } from '../services/token.service';
import {program } from './program';
import { ManageUserService } from '../manage-user/manage-user.service';
import { NotifierService } from 'angular-notifier';
@Component({
  selector: 'app-register-student',
  templateUrl: './register-student.component.html',
  styleUrls: ['./register-student.component.css']
})
export class RegisterStudentComponent implements OnInit {
 public form={
   firstName:null,
   middleName:null,
   lastName:null,
   DOB:null,
   email:null,
   password:null,
   photo:File=null,
  phone:null,
  department:null,
  program:null,
  studentId: null,
  entry:null,
  sex:null

 }
 departments=[]
 imageUrl="";
 programs:any;
 error=null;
 selectedFile: File= null;
 message=null;
 registeredBy=null;
 timeTable=null;
  constructor( private http: HttpClient,private token: TokenService,private UserService: ManageUserService,private notification: NotifierService) { }

  ngOnInit() 
  {
    this.registeredBy=this.token.getUser();
    this.http.get(`http://localhost:8000/registralClerk/student/programs`).subscribe(program => 
    {
      this.programs=program;
      console.log(this.programs);
    }
    );
    this.http.get(`http://localhost:8000/registralClerk/registerStudent`).subscribe(timeTable => this.handleGetResponse(timeTable)
    
    );
    
  }
  handleGetResponse(timeTable)
  {
    this.timeTable=timeTable.message;

  }
  loadDepartments()
  {
    this.UserService.getDepartments().subscribe(departments=>{
      this.departments=departments;
      console.log(this.departments);
    });
  }
  onFileSelected(event){
    this.selectedFile=<File>event.target.files[0];
    // this.form.photo=event.target.files[0];
    var reader=new FileReader();
    reader.onload=(event:any)=>{
      this.imageUrl=event.target.result
    }
    reader.readAsDataURL(this.selectedFile);
   console.log(event);
  }
  onSubmit(){
    // // const headers = new HttpHeaders();
    // // console.log(this.form);
    // // this.form.photo=this.selectedFile;
    // // console.log(this.form);
    // headers.append('Content-Type', 'multipart/form-data');
    //       headers.append('Accept', 'application/json');
    // this.form.append()
    const formData:FormData=new FormData();
    formData.append('photo',this.selectedFile,this.selectedFile.name);
    formData.append('firstName',this.form.firstName);
    formData.append('middleName',this.form.middleName);
    formData.append('lastName',this.form.lastName);
    formData.append(' DOB',this.form. DOB);
    formData.append('email',this.form.email);
    formData.append('password',this.form.password);
    formData.append('phone',this.form.phone);
    formData.append('department',this.form.department);
    formData.append('program',this.form.program);
    formData.append('studentId',this.form.studentId);
    formData.append('entry',this.form.entry);
    formData.append('sex',this.form.sex);
    console.log(formData);
    this.http.post(`http://localhost:8000/registralClerk/${this.registeredBy}/registerStudent`,formData,).subscribe(res => this.handleResponse(res));
    
     
  }
  handleResponse(data)
  {
    console.log("in handle response");
    console.log(data);
    this.message=data.message;
    this.form.firstName="";
    this.form.middleName="";
    this.form.lastName="";
    this.form.password="";
    this.form.phone="";
    this.form.program="";
    this.form.department="";
    this.form.entry="";
    this.form.sex="";
    this.form.entry="";
    this.form.DOB="";
    console.log(data);
    console.log('under response');
    if(data.message==="succesfully")
    {
      
      // this.message="User has been sucessfully registered";
      this.notification.notify("success", "saved Succesfully");
    }
    else{
  
      this.handleError(data.error);
      
    }
    
  }
  
  handleError(error)
  {
    console.log(error);
     this.error=error;
     window.alert(this.error);
  }

}
