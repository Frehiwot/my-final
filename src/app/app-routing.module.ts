import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdvisingInformationComponent} from './advising-information/advising-information.component';
import {LoginComponent } from './login/login.component';
import { ManageUserComponent } from './manage-user/manage-user.component';
import { ManageCourseComponent } from './manage-course/manage-course.component';
import { ManageCourseOfferingComponent } from './manage-course-offering/manage-course-offering.component';
import { AssignInstructorComponent } from './assign-instructor/assign-instructor.component';
import {AuthGuard} from './auth.guard';
import { UserDetailComponent } from './manage-user/user-detail/user-detail.component';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { CourseOfferingDetailComponent } from './course-offering-detail/course-offering-detail.component';
import { ListInstructorsComponent } from './list-instructors/list-instructors.component';
import { RegisterStudentComponent } from './register-student/register-student.component';
import { SemesterComponent } from './semester/semester.component';
import { ListStudentComponent } from './list-student/list-student.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';
import { StudentCourseComponent} from './student-course/student-course.component';
 import {InstructorPage1Component} from './instructor/instructor-page1/instructor-page1.component';
import { InstructorStudentsComponent } from './instructor/instructor-students/instructor-students.component';
import { AllCoursesComponent } from './all-courses/all-courses.component';
import { ManageCourseModuleComponent } from './manage-course-module/manage-course-module.component';
import { ManageProgramComponent } from './manage-program/manage-program.component';
import { ManageGradeComponent } from './manage-grade/manage-grade.component';
import { EditUserComponent } from './manage-user/edit-user/edit-user.component';
import { EditCourseComponent } from './manage-course/edit-course/edit-course.component';
import { EditProgramComponent } from './manage-program/edit-program/edit-program.component';
import { EditModuleComponent } from './manage-course-module/edit-module/edit-module.component';
import { ChatBotComponent } from 'src/app/chat-bot/chat-bot.component';
import { FAQComponent } from './faq/faq.component';
const routes: Routes = [
  {
    path:'',component:LoginComponent,
   
  },
  {
    path:'login',component:LoginComponent,
   
  },
  
  {path:'registralClerk/student', component: RegisterStudentComponent},
  {path:'registralClerk/students', component: ListStudentComponent,canActivate: [AuthGuard]},
  {path:'registralClerk/student/:id', component: StudentDetailComponent,canActivate: [AuthGuard]},
  { path: 'admin/users', component: ManageUserComponent},
  { path: 'admin/user/edit/:id', component: EditUserComponent},
  { path: 'departmentHead/courses', component: ManageCourseComponent, canActivate: [AuthGuard]},
  {path:'departmentHead/course-offer/:id', component: ManageCourseOfferingComponent, canActivate: [AuthGuard]},
  {path: 'departmentHead/course/:id/instructors', component: AssignInstructorComponent,canActivate: [AuthGuard]},
  {path:'departmentHead/:departmentHeadId/course/:courseId/instructor', component: AssignInstructorComponent, canActivate: [AuthGuard]},
  {path:'departmentHead/module/edit/:code', component: EditModuleComponent, canActivate: [AuthGuard]},
  {path:'departmentHead/program/edit/:title', component: EditProgramComponent, canActivate: [AuthGuard]},
  {path:'departmentHead/course/edit/:code', component: EditCourseComponent, canActivate: [AuthGuard]},
  {path: 'admin/user/:id', component: UserDetailComponent,canActivate: [AuthGuard]},
  {path: 'departmentHead/course/:id', component: CourseDetailComponent,canActivate: [AuthGuard]},
  {path: 'instructorDetail/:id', component: CourseOfferingDetailComponent,canActivate: [AuthGuard]},
  {path: 'departmentHead/instructors', component: ListInstructorsComponent,canActivate: [AuthGuard]},
  {path: 'registralClerk/semesters', component: SemesterComponent,canActivate: [AuthGuard]},
  {path:'Student/:id/registered/courses', component: AllCoursesComponent,canActivate: [AuthGuard]},
  {path: 'student/:id/course', component: StudentCourseComponent,canActivate: [AuthGuard]},
  
  {path: 'instructor/:id/courses', component: InstructorPage1Component,canActivate: [AuthGuard]},
  {path: 'course/:id/students', component: InstructorStudentsComponent,canActivate: [AuthGuard]},
  {path: 'departmentHead/offered/courses',component: ManageCourseOfferingComponent,canActivate: [AuthGuard]},
  {path: 'departmentHead/modules',component: ManageCourseModuleComponent,canActivate: [AuthGuard]},
  {path: 'departmentHead/programs',component: ManageProgramComponent,canActivate: [AuthGuard]},
  {path: 'registralClerk/grades',component: ManageGradeComponent,canActivate: [AuthGuard]},
  {path: 'student/:id/advisor',component: ChatBotComponent,canActivate: [AuthGuard]},
  {path: 'faq',component: FAQComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
