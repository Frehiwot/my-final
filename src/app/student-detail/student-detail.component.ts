import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
import {ManageUserService } from '../manage-user/manage-user.service';
import {Student} from './student';
import { User } from '../manage-user/user';
@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent implements OnInit {
  selectedStudent:number
  students:Student
  users:User
  specficStudent=null
  constructor(private httpClient:ManageUserService,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params=>{
       this.selectedStudent=+params.get('id');
       console.log(this.selectedStudent)

      }
    );
    this.httpClient.getStudent(this.selectedStudent).subscribe(student=>{
      this.students=student;
      console.log("under get specfic student");
      console.log(student);
      console.log(this.students);
      if(this.students)
      {
        this.specficStudent=this.students[0];
        console.log(this.specficStudent);
      }

    });
    this.httpClient.getSpecficUser(this.selectedStudent).subscribe(user=>
      {
        console.log("under get specfic user");
        console.log(user);
        this.users=user
      }

    )
    // console.log(this.students);
  }

}
