import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }
  handle(token,userId,userType)
  {
    this.set(token,userId,userType);
    console.log(this.isValid());
  }
  set(token,userId,userType)
  {
    localStorage.setItem('token',token);
    localStorage.setItem('userId',userId);
    localStorage.setItem('userType',userType);
  }
  get(){
    return localStorage.getItem('token');
  }
  getUser(){
    return localStorage.getItem('userId');
  }
  getUserType(){
    return localStorage.getItem('userType');
  }
  remove(){
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('userType');
  }

  isValid(){
    const token=this.get();
    if(token)
    {
      const payload = this.payload(token);
      if(payload)
      {
        return payload.iss == `http://localhost:8000/api/login`? true:false;
      }
      
    }
    return false;
  }

  payload(token){
   const payload= token.split('.')[1];
   return this.decode(payload);
  }
  decode(payload)
  {
    return JSON.parse(atob(payload));
  }
  loggedIn(){
    return this.isValid();
  }

}
