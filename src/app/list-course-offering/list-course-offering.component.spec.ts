import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCourseOfferingComponent } from './list-course-offering.component';

describe('ListCourseOfferingComponent', () => {
  let component: ListCourseOfferingComponent;
  let fixture: ComponentFixture<ListCourseOfferingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCourseOfferingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCourseOfferingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
