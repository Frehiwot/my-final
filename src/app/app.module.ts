import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http'; 
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ManageCourseComponent } from './manage-course/manage-course.component';
import { LoginComponent } from './login/login.component';
import { AdvisingInformationComponent } from './advising-information/advising-information.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { ManageUserComponent } from './manage-user/manage-user.component';
import { ManagePasswordComponent } from './manage-password/manage-password.component';
import { ManagePersonalAccountComponent } from './manage-personal-account/manage-personal-account.component';
import { ManageCourseOfferingComponent } from './manage-course-offering/manage-course-offering.component';
import { AssignInstructorComponent } from './assign-instructor/assign-instructor.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
 import {NgSelectModule} from '@ng-select/ng-select';
 import { NotifierModule } from "angular-notifier";

import { CourseDetailComponent } from './course-detail/course-detail.component';
import { CourseOfferingDetailComponent } from './course-offering-detail/course-offering-detail.component';
import { ListCourseOfferingComponent } from './list-course-offering/list-course-offering.component';
import { ListInstructorsComponent } from './list-instructors/list-instructors.component';
import { UserDetailComponent } from './manage-user/user-detail/user-detail.component';
import { RegisterStudentComponent } from './register-student/register-student.component';
import { SemesterComponent } from './semester/semester.component';
import { ListStudentComponent } from './list-student/list-student.component';
import { StudentCourseComponent } from './student-course/student-course.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';

import {InstructorPage1Component} from './instructor/instructor-page1/instructor-page1.component';
import { InstructorStudentsComponent } from './instructor/instructor-students/instructor-students.component';
import { GradeReportComponent } from './grade-report/grade-report.component';
import { SetDateComponent } from './set-date/set-date.component';
import { ManageCourseModuleComponent } from './manage-course-module/manage-course-module.component';
import { AllCoursesComponent } from './all-courses/all-courses.component';
import { Header2Component } from './header2/header2.component';
import { NotificationComponent } from './notification/notification.component';
import { ManageProgramComponent } from './manage-program/manage-program.component';
import { ManageGradeComponent } from './manage-grade/manage-grade.component';
import { EditUserComponent } from './manage-user/edit-user/edit-user.component';
import { EditCourseComponent } from './manage-course/edit-course/edit-course.component';
import { EditProgramComponent } from './manage-program/edit-program/edit-program.component';
import { EditModuleComponent } from './manage-course-module/edit-module/edit-module.component';

import { NgxPaginationModule } from 'ngx-pagination';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ChatBotComponent } from 'src/app/chat-bot/chat-bot.component';
import { FAQComponent } from './faq/faq.component';

@NgModule({
  declarations: [
    AppComponent,
    ManageCourseComponent,
    LoginComponent,
    AdvisingInformationComponent,
    HeaderComponent,
    ManageUserComponent,
    ManagePasswordComponent,
    ManagePersonalAccountComponent,
    ManageCourseOfferingComponent,
    AssignInstructorComponent,
    EditProgramComponent,
    CourseDetailComponent,
    CourseOfferingDetailComponent,
    ListCourseOfferingComponent,
    ListInstructorsComponent,
    UserDetailComponent,
    RegisterStudentComponent,
    SemesterComponent,
    ListStudentComponent,
    StudentCourseComponent,
    StudentDetailComponent,
    InstructorPage1Component,
    InstructorStudentsComponent,
    GradeReportComponent,
    SetDateComponent,
    ManageCourseModuleComponent,
    AllCoursesComponent,
    Header2Component,
    NotificationComponent,
    ManageProgramComponent,
    ManageGradeComponent,
    EditUserComponent,
    EditCourseComponent,
    EditModuleComponent,
    ChatBotComponent,
    FAQComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NotifierModule.withConfig({
      position: {
        horizontal: { position: 'right' },
        vertical: { position: 'top' }
      }
    }),
    NgxPaginationModule,
    Ng2SearchPipeModule,
    ReactiveFormsModule,
    TooltipModule.forRoot(),
    NgSelectModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
