import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course} from './course';
@Injectable({
  providedIn: 'root'
})
export class GradeReportService {

  constructor(private httpClient: HttpClient) { }
  public getReport(studentId):Observable<Course[]>
  {
     return this.httpClient.get<Course[]>(`http://localhost:8000/student/${studentId}/report`);
  }
}
