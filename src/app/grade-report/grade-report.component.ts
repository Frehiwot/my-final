import { Component, OnInit } from '@angular/core';
import {GradeReportService} from './grade-report.service';
import {Course } from './course';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
@Component({
  selector: 'app-grade-report',
  templateUrl: './grade-report.component.html',
  styleUrls: ['./grade-report.component.css']
})
export class GradeReportComponent implements OnInit {
   studentId:number;
   courses:Course[]
  constructor(private reportService: GradeReportService,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params=>{
        this.studentId=+params.get('id');
        console.log(this.studentId)

      }
    );
    this.reportService.getReport(this.studentId).subscribe(courses=>this.handelResponse(courses));

    
  }
  handelResponse(courses)
  {
    console.log(courses);
    this.courses=courses;
  }

}
