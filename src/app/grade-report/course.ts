export class Course{
    id:number;
    code:string;
    title:string;
    lectureHours:number;
    laboratoryHours:number;
    creditHours:number;
    ECTS:number;
    offered:number;
    requisite:string;
    module:string;
    result:number;
    grade:string;
}