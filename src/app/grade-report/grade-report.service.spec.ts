import { TestBed } from '@angular/core/testing';

import { GradeReportService } from './grade-report.service';

describe('GradeReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GradeReportService = TestBed.get(GradeReportService);
    expect(service).toBeTruthy();
  });
});
