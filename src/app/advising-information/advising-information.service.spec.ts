import { TestBed } from '@angular/core/testing';

import { AdvisingInformationService } from './advising-information.service';

describe('AdvisingInformationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvisingInformationService = TestBed.get(AdvisingInformationService);
    expect(service).toBeTruthy();
  });
});
