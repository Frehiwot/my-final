import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvisingInformationComponent } from './advising-information.component';

describe('AdvisingInformationComponent', () => {
  let component: AdvisingInformationComponent;
  let fixture: ComponentFixture<AdvisingInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvisingInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvisingInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
