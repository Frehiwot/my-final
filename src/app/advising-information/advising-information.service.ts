import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {AdvisingInformation } from '../advising-information/advising-information';

@Injectable({
  providedIn: 'root'
})
export class AdvisingInformationService {
API_SERVER='http://localhost:8000';
  constructor(private httpClient: HttpClient) { }
  information():Observable<AdvisingInformation[]>{
    return this.httpClient.get<AdvisingInformation[]>(`${this.API_SERVER}/list`);
  }
  addInformation(information: AdvisingInformation):Observable<AdvisingInformation>{
    console.log("hellow wprld");
    return this.httpClient.post<AdvisingInformation>(`${this.API_SERVER}/store`,information);
  }
}
