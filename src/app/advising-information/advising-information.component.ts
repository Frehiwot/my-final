import { Component, OnInit } from '@angular/core';
import {AdvisingInformationService} from '../advising-information/advising-information.service';
import { AdvisingInformation } from '../advising-information/advising-information';
@Component({
  selector: 'app-advising-information',
  templateUrl: './advising-information.component.html',
  styleUrls: ['./advising-information.component.css']
})
export class AdvisingInformationComponent implements OnInit {
  informations: AdvisingInformation[];
  constructor(private informationService: AdvisingInformationService) { }

  ngOnInit() {
    this.informationService.information().subscribe((informations:AdvisingInformation[])=>{
      this.informations=informations;
      console.log(this.informations);
    })
  }
  add(informations: AdvisingInformation):void{
    console.log(informations);
    this.informationService.addInformation(informations).subscribe(
      information => this.informations.push(information)
    );
  }

}
