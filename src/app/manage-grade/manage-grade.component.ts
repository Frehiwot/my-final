import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import Swal from 'sweetalert2/dist/sweetalert2.all.min.js'

@Component({
  selector: 'app-manage-grade',
  templateUrl: './manage-grade.component.html',
  styleUrls: ['./manage-grade.component.css']
})
export class ManageGradeComponent implements OnInit {
 grades=[];
//  departmentId=null;
//  departments=[]
 addButton=null
 public form={
  name:null,
  value:null,
  minValue:null,
  maxValue:null,
  status:null,
 
};
total=0;
  page=1;
  pageSize=5
updatting=false;

message=null;
clerkId=null
grade=null;
expanded=[];
  constructor(private httpClient:HttpClient,private Token: TokenService,private router: Router,private notification: NotifierService) { }

  ngOnInit() {
    this.clerkId=this.Token.getUser();
    this.loadGrades();
     this.total=this.grades.length;
    // this.httpClient.get<[]>(`http://localhost:8000/list/departments`).subscribe(departments=>this.departments=departments);

  }
  loadGrades()
  {
    this.httpClient.get<[]>(`http://localhost:8000/registralClerk/grades`).subscribe(grades=>
    {
      this.grades=grades;
      console.log(this.grades);
      for(var i:number=0;i<this.grades.length;i++)
      {
        this.expanded[i]=false;
  
      }

    });
  }
  pageChanged(event){
    this.page = event;
  }
  onSubmit(){
    console.log(this.form);
    this.httpClient.post(`http://localhost:8000/registralClerk/store/grade`,this.form).subscribe(grade=>
    {
      console.log(grade);
      this.handlePost(grade)
    }
    );
  }
  onSubmitEdited(){
    console.log(this.form);
    let gradeResponse=null
    this.httpClient.put(`http://localhost:8000/registralClerk/${this.clerkId}/grade/${this.grade.name}/edit`,this.form).subscribe(grade=>
    {
      console.log(grade);
      gradeResponse=grade;
      this.updatting=false;
      this.addButton=null;
      if(gradeResponse.message)
      {
        this.notification.notify("success", "Saved Succesfully"); 
        this.loadGrades();
      }
      else if(gradeResponse.error)
      {
        this.notification.notify("warning", gradeResponse.error); 

      }
     
     
     
    }
    );
  }
  handlePost(grade)
  {
    if(grade.message)
    {
      this.notification.notify("success", "Saved Succesfully"); 
      this.grades.push(grade.grade);
    }
    else if(grade.error)
    {
      this.notification.notify("warning", grade.error); 

    }

  }
  show(){
    console.log("add button clicked");
    if(!this.addButton)
    {
        console.log("addButton is null");
        this.addButton="show";
        
    }
    else{
      this.addButton=null;
    }
  }
  toggleFloat(i:number)
  {
    console.log("under toggle");
    console.log(this.expanded[i]);
    this.expanded[i]=!this.expanded[i];
    console.log(this.expanded[i]);
  }
  delete(grade)
  {
    

    console.log("under deleting");
    console.log(grade);
    let responsee=null;
    
    let deletionResponse=null;
    Swal.fire({
      confirmButtonColor: '#2A66A7',
      title: "Are you sure you want to delete this offering",


      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: "Ok",
      cancelButtonText: "cancel"
    }).then((result) => {
      if (result.value) {
        this.httpClient.request('delete',`http://localhost:8000/departmentHead/${this.clerkId}/grade/${grade.name}/delete`).subscribe((response)=>{
        console.log(response);
        responsee=response;
        if(responsee.message)
        {
          this.notification.notify("success", " Deletion Succesfully"); 
          this.loadGrades();
         
        }
        else if(responsee.error)
        {
          this.notification.notify("warning", responsee.error);
        }
       
    });
       
       

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })

  }
  edit(grade)
  {
    this.loadGrade(grade);
    this.updatting=true;
   this.addButton="show";
  }
  loadGrade(grade)
  {
    this.httpClient.get(`http://localhost:8000/departmentHead/${this.clerkId}/grade/${grade.name}`).subscribe((response)=>{
      console.log(response);
      this.grade=response;
    this.form={
        name:this.grade.name,
        value:this.grade.value,
        minValue:this.grade.minValue,
        maxValue:this.grade.maxValue,
        status:this.grade.status,
       
      };
      
    });
  }

}
