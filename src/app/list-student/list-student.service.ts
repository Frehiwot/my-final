import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Student} from './student';

import { Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ListStudentService {

  constructor(private httpClient: HttpClient) { }
  getStudents():Observable<Student[]> {
    return this.httpClient.get<Student[]>(`http://localhost:8000/registralClerk/Students`);
   }
  
   deleteStudent(id: number): Observable<{}>{
   return this.httpClient.get(`http://localhost:8000/registralClerk/deleteStudent/${id}`);
   }
   updateStudent(Student: Student):Observable<Student>{
   return this.httpClient.put<Student>(`http://localhost:8000/registralClerk/updateStudent/${Student.id}`,Student);
   }
}
