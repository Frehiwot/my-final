import { Component, OnInit } from '@angular/core';

import { ListStudentService } from './list-student.service';
import { Student } from './student';

@Component({
  selector: 'app-list-student',
  templateUrl: './list-student.component.html',
  styleUrls: ['./list-student.component.css']
})
export class ListStudentComponent implements OnInit {

  Students: Student[]
  editStudent: Student
  listButton=null;
  addButton=null;
  message:null
  expanded=[];
  total=0;
 page=1;
 pageSize=5
  
  constructor(private studentService: ListStudentService) { }

  ngOnInit() {
    console.log("on init");
    this.loadStudents();
    this.total=this.Students.length;
    
  }
  loadStudents()
  {
    this.studentService.getStudents().subscribe(
      Student=>this.Students=Student
    );
  }
 
handleGetResponse(Student)
{
  
  this.Students=Student;
  
  for(var i:number=0;i<this.Students.length;i++)
    {
      this.expanded[i]=false;

    }
  }
delete(Student: Student): void{
console.log(Student);
this.studentService.deleteStudent(Student.id).subscribe();
}
toggleFloat(i:number)
{
  this.expanded[i]=!this.expanded[i];
}
edit(Student)
{
    this.editStudent=Student;
}

update(Student: Student): void{
    if(this.editStudent)
    {
      this.studentService.updateStudent(this.editStudent).subscribe(Student => {
        const ix = Student ? this.Students.findIndex(h => h.id == Student.id) : -1
        if(ix > -1){
          this.Students[ix]=Student
        }
      })
      this.editStudent = undefined
    }
}
listButtons(){
  console.log("Actions Button  clicked");
  if(!this.listButton)
  {
      console.log("listButton is null");
      this.listButton="show";
      
  }
  else{
    this.listButton=null;
  }
}

show(){
  console.log("add button clicked");
  if(!this.addButton)
  {
      console.log("addButton is null");
      this.addButton="show";
      
  }
  else{
    this.addButton=null;
  }
}
pageChanged(event){
  this.page = event;
}

}
