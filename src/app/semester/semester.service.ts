import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Semester } from './semester';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SemesterService {

  constructor(private httpClient: HttpClient) { }
  getSemesters(registeredBy):Observable<Semester[]> {
    return this.httpClient.get<Semester[]>(`http://localhost:8000/registralClerk/${registeredBy}/Semesters`);
   }
   addSemester(Semester: Semester,registeredBy): Observable<Semester>{
   
   return this.httpClient.post<Semester>(`http://localhost:8000/registralClerk/${registeredBy}/registerSemester `,Semester);
   }
   deleteSemester(id: number,registeredBy): Observable<{}>{
   return this.httpClient.get(`http://localhost:8000/registralClerk/deleteSemester/${id}`);
   }
   updateSemester(Semester: Semester,registeredBy):Observable<Semester>{
   return this.httpClient.put<Semester>(`http://localhost:8000/registralClerk/updateSemester/${Semester.id}`,Semester);
   }
   

}
