export class Semester{
    id:number;
    semester:number;
    year:number;
    startedOn:Date;
    endedOn:Date;
    registrationStarts:Date;
    registrationEnds:Date;
    semesterLoad:number;
    
}