import { Component, OnInit } from '@angular/core';
import {SemesterService } from './semester.service';
import { Semester} from './semester';
import { TokenService } from '../services/token.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-semester',
  templateUrl: './semester.component.html',
  styleUrls: ['./semester.component.css']
})
export class SemesterComponent implements OnInit {
  Semesters: Semester[]
  editSemester: Semester
  message:null
  addButton=null
  registeredBy=null
  listButton=null;

  constructor(private semester: SemesterService,private token: TokenService,private notification: NotifierService) { }

  ngOnInit() {
    this.registeredBy=this.token.getUser();
    this.semester.getSemesters(this.registeredBy).subscribe(
      Semester=>{
        console.log(Semester);
        this.Semesters=Semester;
      }
    );
    console.log(this.token.getUser());

  }
  add(Semester: Semester): void{
    this.editSemester = undefined;
    console.log(Semester);
    this.semester.addSemester(Semester,this.registeredBy).subscribe(
    Semester => this.handleResponse(Semester)
     
     );
}
handleResponse(semester){

  console.log("after saving");
  console.log(semester);

  if(semester.message)
  {
    
    // this.message="User has been sucessfully registered";
    this.notification.notify("success", "saved Succesfully");
    this.Semesters.push(semester.semester);
  }
  else if(semester.error){

    this.notification.notify("warning",semester.error);
    
  }

 
  // this.message=semester.message;


}
delete(Semester: Semester): void{
  console.log(Semester);
  this.semester.deleteSemester(Semester.id,this.registeredBy).subscribe();
  }
  edit(Semester)
  {
      this.editSemester=Semester;
  }
  
  update(Semester: Semester): void{
      if(this.editSemester)
      {
        this.semester.updateSemester(this.editSemester,this.registeredBy).subscribe(Semester => {
          const ix = Semester ? this.Semesters.findIndex(h => h.id == Semester.id) : -1
          if(ix > -1){
            this.Semesters[ix]=Semester
          }
        })
        this.editSemester = undefined
      }
  }

  show(){
    console.log("add button clicked");
    if(!this.addButton)
    {
        console.log("addButton is null");
        this.addButton="show";
        
    }
    else{
      this.addButton=null;
    }
  }


}
