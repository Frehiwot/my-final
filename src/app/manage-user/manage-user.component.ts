import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ManageUserService } from './manage-user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { User } from './user';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';
import Swal from 'sweetalert2/dist/sweetalert2.all.min.js'
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.css']
})
export class ManageUserComponent implements OnInit {
  title = 'ngrx';
  showSpinner = true;
  users: User[]
  editUser: User
  selectedDepartment: string
  message=null
  addButton=null
  adminId=null
  updateBtn=null
  listButton=null;
  registrationForm:FormGroup;
  submitted= false;
  public error=null;
  departments=[]
  expanded=[];
  config: any;
  total=0;
page=1;
pageSize=5

  constructor(private formBuilder:FormBuilder,
    private UserService: ManageUserService,private token: TokenService,private route:ActivatedRoute,private router:Router,private notification: NotifierService) { }

  ngOnInit() {
    this.router.navigateByUrl('/admin/users');
    this.registrationForm=this.formBuilder.group({
      firstName: ['',Validators.required],
      middleName: ['',Validators.required],
      lastName: ['',Validators.required],
      department:['',Validators.required],
      userId:['',[Validators.required,Validators.minLength(10),Validators.maxLength(11)]],
      phone:['',[Validators.required,Validators.minLength(10),Validators.maxLength(10)]],
      phone2:['',[Validators.minLength(10),Validators.maxLength(10)]],
      email:['',[Validators.required,Validators.email]],
      role:['',Validators.required],
      password:['',[Validators.required,Validators.minLength(6)]]
    });
    this.adminId=this.token.getUser();
  this.loadDepartments()
   this.loadUsers();
   this.config = {
    itemsPerPage: 10,
    currentPage: 1,
    totalItems: this.users.length
  };
  this.total=this.users.length;
   
    console.log(this.token.getUser());
  }
  loadUsers()
  {
    this.UserService.getUsers(this.adminId).subscribe(
      user=>this.handleGetResponse(user)
      
    );
  }
  pageChanged(event){
    this.page = event;
  }
  handleGetResponse(user)
  {
    
      this.users=user;
      this.showSpinner = false;
      console.log(this.users);
      for(var i:number=0;i<this.users.length;i++)
      {
        this.expanded[i]=false;
  
      }
    
  }
  clear()
  {
    console.log("under clear");
    this.registrationForm.get('firstName').setValue('');
    this.registrationForm.get('middleName').setValue('');
    this.registrationForm.get('lastName').setValue('');
    this.registrationForm.get('department').setValue('');
    this.registrationForm.get('userId').setValue('');
    this.registrationForm.get('phone').setValue('');
    this.registrationForm.get('phone2').setValue('');
    this.registrationForm.get('email').setValue('');
    this.registrationForm.get('role').setValue('');
    this.registrationForm.get('password').setValue('');
  }
  loadDepartments()
  {
    this.UserService.getDepartments().subscribe(departments=>{
      this.departments=departments;
      console.log(this.departments);
    });
  }
  onSubmit(){
      console.log('under submitter');
      this.submitted=true;
      if(this.registrationForm.invalid)
      {
        return;
      }
      else{
        console.log(this.registrationForm.value);
        this.add(this.registrationForm.value);
      }
  }
  get f(){
    return this.registrationForm.controls;
  }
  add(user: User): void{
    this.editUser = undefined;
    console.log(user);
    this.UserService.addUser(user,this.adminId).subscribe(
    user => this.handleResponse(user),
    error=> this.handleError(error)
     
     );
}
handleResponse(user){
  console.log(user);
  console.log('under response');
  if(user.message === "succesfully")
  {
    this.users.push(user.user);
    this.notification.notify("success", user.firstName+ "  saved Succesfully");
  }
  else{

    this.handleError(user.error);
    
  }
 
  
}
handleError(error)
{
  console.log(error);
   this.error=error;
   window.alert(this.error);
}
listButtons(){
  console.log("Actions Button  clicked");
  if(!this.listButton)
  {
      console.log("listButton is null");
      this.listButton="show";
      
  }
  else{
    this.listButton=null;
  }
}
toggleFloat(i:number)
{
  this.expanded[i]=!this.expanded[i];
}
handle(user)
{
  this.users.push(user.user);
  // this.message=User.message;
}

delete(User: User): void{
    console.log(User);
    let deletionResponse=null;
    Swal.fire({
      confirmButtonColor: '#2A66A7',
      title: "Are u sure u want to delete",


      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: "Ok",
      cancelButtonText: "cancel"
    }).then((result) => {
      if (result.value) {
        this.UserService.deleteUser(User.id).subscribe(response=>{
          console.log("after deleting");
          console.log(response);
          deletionResponse=response
          if(deletionResponse.message)
          {
            this.notification.notify("success", " Deletion Succesfully");
            this.loadUsers();
          }
          else if(deletionResponse.error)
          {
            this.notification.notify("warning", deletionResponse.error);
          }
          
        
        });

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })

}
edit(User)
{
    this.editUser=User;
    // this.addButton="show";
    // this.updateBtn="update";

}

update(User: User): void{
    console.log('in update method');
    if(this.editUser)
    {
      this.UserService.updateUser(this.editUser,this.adminId).subscribe(user => {
        const ix = user ? this.users.findIndex(h => h.id == user.id) : -1
        if(ix > -1){
          this.users[ix]=user
        }
        console.log(user)
      })
      this.editUser = undefined
    }
    // this.UserService.updateUser(User,this.adminId).subscribe(users=>console.log(users));
}

show(){
  console.log("add button clicked");
  if(!this.addButton)
  {
      console.log("addButton is null");
      this.addButton="show";
      
  }
  else{
    this.addButton=null;
  }
}

}
