import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders } from '@angular/common/http';
import {Observable,throwError} from 'rxjs';
import { retry,catchError } from 'rxjs/operators';
import { User } from './user';
import { Student } from '../student-detail/student';
@Injectable({
  providedIn: 'root'
})
export class ManageUserService {

  constructor( private httpClient: HttpClient){ }
	getUsers(adminId):Observable<User[]> {
	
	 return this.httpClient.get<User[]>(`http://localhost:8000/admin/${adminId}/Users`).pipe(
		 retry(1),
		 catchError(this.handleError)
	 );
	}

	getSpecficUser(userId):Observable<User>{
		console.log("specfic user id");
		console.log(userId);
		return this.httpClient.get<User>(`http://localhost:8000/user/${userId}`);
	}

	addUser(User: User,adminId): Observable<User>{
	console.log(User);
	return this.httpClient.post<User>(`http://localhost:8000/admin/${adminId}/registerUser`,User).pipe(
		retry(1),
		catchError(this.handleError)
	);
	}
	deleteUser(id: number): Observable<{}>{
	return this.httpClient.request('delete',`http://localhost:8000/admin/user/${id}`);
	}
	updateUser(User,adminId):Observable<User>{
	return this.httpClient.put<User>(`http://localhost:8000/admin/${adminId}/user/${User.id}`,User);
	}
	getStudent(studentId):Observable<Student>{
		return this.httpClient.get<Student>(`http://localhost:8000/registralClerk/Student/${studentId}`);
	}
	getDepartments()
	{
		return this.httpClient.get<[]>(`http://localhost:8000/list/departments`);
	}

	handleError(error)
	{
		let errorMessage='';

		if(error.error instanceof ErrorEvent)
		{
		//   clien-side error
			errorMessage= `Error: ${error.error.message}`;
		}
		else{
			errorMessage= `Error Code: ${error.status}\nMessage: ${error.message}`;
		}
		window.alert(errorMessage);
		return throwError(errorMessage);
	}

}
