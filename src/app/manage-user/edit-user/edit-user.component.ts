import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ManageUserService } from '../manage-user.service';
import { FormBuilder, FormControlName, FormGroup, Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import { User } from '../user';
import { Observable } from 'rxjs';
import { TokenService } from '../../services/token.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
    registrationForm:FormGroup;
    message=null;
    specficUserId=null
    users=null
    adminId=null;
    departments=[];
    form={
        id:0,
        firstName: 'abebe',
        middleName: '',
        lastName: '',
        department:'',
        userId:'',
        phone:0,
        phone2:0,
        email:'',
        role:'',
        password:''
      };
    
  constructor(private formBuilder:FormBuilder,
    private UserService: ManageUserService,private token: TokenService,private route:ActivatedRoute,private router:Router,private notification: NotifierService) { 
        

    }

  ngOnInit() {
   
    this.adminId=this.token.getUser();
    this.route.paramMap.subscribe(
      params=>{
        this.specficUserId=+params.get('id');
       console.log( this.specficUserId)

      }
    );
    this.form.id=this.specficUserId;
    this.loadDepartmens();
    this.loadUser();
    
  
    


  }
  loadDepartmens()
  {
    this.UserService.getDepartments().subscribe(departments=>{
      this.departments=departments;
      console.log(this.departments);
    });
  }
  loadUser()
  {
   
    this.UserService.getSpecficUser(this.specficUserId).subscribe(
        user=>{
            this.users=user;
            console.log(this.users);
            this.form={
                id:this.users.id,
                firstName: this.users.firstName,
                middleName: this.users.middleName,
                lastName: this.users.lastName,
                department:this.users.department,
                userId:this.users.userId,
                phone:this.users.primaryPhoneNumber,
                phone2:this.users.secondaryPhoneNumber,
                email:this.users.email,
                role:this.users.role,
                password:this.users.password
              };  
        }
        
      );

  }
  onSubmit(){
    console.log('under submitter');
    this.UserService.updateUser(this.form,this.adminId).subscribe(user=>{
      console.log(user);
      if(user)
      {
        this.handleResponse(user);
      }
  
    });
}
handleResponse(user)
{
  console.log("under response")
  if(user.message == 'succesfull')
  {
    console.log("inside message succefuk")
    this.notification.notify("success", user.user.firstName+ "  saved Succesfully");
    
  }
  else if(user.error)
  {
    this.notification.notify("warning", user.error);
  }
 
}

}