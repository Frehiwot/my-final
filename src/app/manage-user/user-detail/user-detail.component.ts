import { Component, OnInit } from '@angular/core';
import {ManageUserService } from '../manage-user.service';
import { User} from '../user';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
   users: User
   selectedUser: number
  constructor(private userService: ManageUserService,private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params=>{
       this. selectedUser=+params.get('id');
       console.log(this. selectedUser)

      }
    );
    this.userService.getSpecficUser(this.selectedUser).subscribe(
      user=>this.users=user
    );
  }

}
