export class User{
     id: number;
    userId: string;
	firstName: string;
	middleName:string;
	lastName:string;
	department:string;
	phone:number;
	phone2:number;
	email: string;
	role: string;
	password: string;
}