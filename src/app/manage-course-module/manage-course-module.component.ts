import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import Swal from 'sweetalert2/dist/sweetalert2.all.min.js'

@Component({
  selector: 'app-manage-course-module',
  templateUrl: './manage-course-module.component.html',
  styleUrls: ['./manage-course-module.component.css']
})
export class ManageCourseModuleComponent implements OnInit {
 modules=[];
 departmentId=null;
 departments=[]
 addButton=null
 total=0;
 page=1;
 pageSize=5
  constructor(private httpClient:HttpClient,private Token: TokenService,private router: Router,private notification: NotifierService) { }

  ngOnInit() {
    this.departmentId=this.Token.getUser();
    this.loadModules();

    this.loadDepartments();
    this.total=this.modules.length;
    

  }
  loadModules()
  {
    this.httpClient.get<[]>(`http://localhost:8000/departmentHead/${this.departmentId}/modules`).subscribe(modules=>{
      console.log(modules);
      this.modules=modules}
      );

  }
  pageChanged(event){
    this.page = event;
  }
  loadDepartments()
  {
    this.httpClient.get<[]>(`http://localhost:8000/list/departments`).subscribe(departments=>this.departments=departments);
  }
  add(module): void{
    //this.editSemester = undefined;
    console.log(module);
    let saveResponse=null;
   
     this.httpClient.post(`http://localhost:8000/departmentHead/${this.departmentId}/register/module`,module).subscribe(modules=>
     {
       console.log(modules);
       saveResponse=modules;
       if(saveResponse.message)
       {
        this.notification.notify("success", " saved Succesfully"); 
        this.modules.push(modules)

       }
       else if(saveResponse.error)
       {
        this.notification.notify("warning", saveResponse.error);

       }
       
     }
     );
  }
  show(){
    console.log("add button clicked");
    if(!this.addButton)
    {
        console.log("addButton is null");
        this.addButton="show";
        
    }
    else{
      this.addButton=null;
    }
  }
  delete(module){
      
      console.log("under deleting");
    console.log(module);
    let responsee=null;
    
    let deletionResponse=null;
    Swal.fire({
      confirmButtonColor: '#2A66A7',
      title: "Are you sure you want to delete this module",


      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: "Ok",
      cancelButtonText: "cancel"
    }).then((result) => {
      if (result.value) {
        this.httpClient.request('delete',`http://localhost:8000/departmentHead/${this.departmentId}/module/${module.moduleCode}/delete`).subscribe((response)=>{
          console.log(response);
          deletionResponse=response;
          if(deletionResponse.message)
          {
            this.notification.notify("success", " Deletion Succesfully"); 
            this.loadModules();
            this.loadDepartments();

          }
          else if(deletionResponse.error)
          {
            this.notification.notify("warning", responsee.error);

          }
          
      });

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })
    

  }

}
