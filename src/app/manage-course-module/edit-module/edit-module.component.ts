import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../../services/token.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-edit-module',
  templateUrl: './edit-module.component.html',
  styleUrls: ['./edit-module.component.css']
})
export class EditModuleComponent implements OnInit {
    module=null;
    departmentId=null;
    departments=[]
    addButton=null
    selectedModule=null;
    form={
       moduleCode:null,
       moduleTitle:null,
       department:null
    }
    message=null
    Response=null
  constructor(private httpClient:HttpClient,private Token: TokenService,private route: ActivatedRoute,private router: Router,private notification: NotifierService) { }

  ngOnInit() {
    this.departmentId=this.Token.getUser();
    console.log(this.departmentId);
    this.route.paramMap.subscribe(
        params=>{
          this.selectedModule=params.get('code').trim();
         console.log( this.selectedModule);
  
        }
      );
      this.loadModule();
      this.loadDepartments();
  }
  loadModule()
  {
    this.httpClient.get(`http://localhost:8000/departmentHead/${this.departmentId}/module/${this.selectedModule}`).subscribe(module=>
    {
        console.log(module);
        this.module=module;
        this.form={
            moduleCode:this.module.moduleCode,
            moduleTitle:this.module.moduleTitle,
            department:this.module.department
         }
       
       
    });
  }
  loadDepartments()
  {
    this.httpClient.get<[]>(`http://localhost:8000/list/departments`).subscribe(departments=>this.departments=departments);
  }
  onSubmit()
  {
      console.log(this.form);
      return this.httpClient.put(`http://localhost:8000/departmentHead/${this.departmentId}/module/edit/${this.selectedModule}`,this.form).subscribe((response)=>{
          console.log(response);
         this.Response=response;
         if(this.Response.message)
         {
            this.notification.notify("success", "saved Succesfully"); 
            //  this.message=this.Response.message;
         }
         else if(this.Response.error)
         {
          this.notification.notify("warning", this.Response.error);
         }
      });
  }

}