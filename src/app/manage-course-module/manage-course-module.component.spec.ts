import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCourseModuleComponent } from './manage-course-module.component';

describe('ManageCourseModuleComponent', () => {
  let component: ManageCourseModuleComponent;
  let fixture: ComponentFixture<ManageCourseModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCourseModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCourseModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
