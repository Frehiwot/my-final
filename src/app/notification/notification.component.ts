import { Component, OnInit } from '@angular/core';
import {NotificationService} from './notification.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { TokenService } from '../services/token.service';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  userId=null
  notifications=[]
  constructor(private notificationService:NotificationService,private route:ActivatedRoute,private router:Router,private token: TokenService) { }

  ngOnInit() {
    this.userId=this.token.getUser();
    this.notificationService.getNotifications(this.userId).subscribe(
      notification=>{
          console.log(notification);
          this.notifications=notification
      }
      
    );

  }

}
