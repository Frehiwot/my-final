import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders } from '@angular/common/http';
import {Observable,throwError} from 'rxjs';
import { retry,catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private httpClient: HttpClient) { }

  getNotifications(userId):Observable<[]> {
	
    return this.httpClient.get<[]>(`http://localhost:8000/notifications/${userId}/student`);
   }
   

}
