import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePersonalAccountComponent } from './manage-personal-account.component';

describe('ManagePersonalAccountComponent', () => {
  let component: ManagePersonalAccountComponent;
  let fixture: ComponentFixture<ManagePersonalAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePersonalAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePersonalAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
