export class ManageCourse{
    id:number;
    code:string;
    title:string;
    lectureHours:number;
    laboratoryHours:number;
    creditHours:number;
    ECTS:number;
    offered:number;
    requisite:string;
    module:string;
    // registeredBy:number;
    // updatedOn:number;
}