import { Component, OnInit } from '@angular/core';
import { ManageCourse } from '../manage-course';
import {ManageCourseService} from '../manage-course.service';
import {ActivatedRoute, Router} from '@angular/router';
import { TokenService } from '../../services/token.service';
import {ManageModule} from '../../manage-course-module/module';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.css']
})
export class EditCourseComponent implements OnInit {
    courses: ManageCourse[];
    modules:ManageModule[];
    editCourse=null;
    addButton=null;
    listButton=null;
    departmentId=null;
    error=null;
    message=null;
    specficCouseCode=null;
    //selected=[];
    public form={
      code:null,
      title:null,
      lectureHours:null,
      laboratoryHours:null,
      requisite:[],
      creditHours:null,
      ects:null,
      module:null
    };
  constructor(private courseService: ManageCourseService,private token: TokenService,private route:ActivatedRoute,private router:Router,private notification: NotifierService) { }

  ngOnInit() {
    this.departmentId=this.token.getUser();
    this.route.paramMap.subscribe(
      params=>{
        this.specficCouseCode=params.get('code');
       console.log( this.specficCouseCode);

      }
    );
    this.loadCourse();
    this.loadModules();
    this.loadCourses();
  }
  onSubmit(){
    console.log(this.form);
   
    this.editCourse=undefined
   this.courseService.updateCourse(this.form,this.departmentId).subscribe(
     course=>this.handleResponse(course)
   );
  }
  loadModules()
  {
    this.courseService.getModule(this.departmentId).subscribe(
      module=>{
        this.modules=module;
        console.log(this.modules);
      }
    );

  }
  handleResponse(course)
  {
    // if(course.message === "succesfull")
    // {
    //   this.courses.push(course.course);
    // }
    // else{
    //     this.handleError(course.error);
    // }
    if(course.message)
    {
      this.notification.notify("success", " You have saved succesfully"); 
    }
    if(course.error)
    {
      this.notification.notify("warning", course.error); 
    }
    console.log(course);
  }
  handleError(error)
  {
    console.log(error);
    this.error=error;
    window.alert(this.error);
  }
  loadCourse()
  {
      this.courseService.getSpecficCourse(this.specficCouseCode).subscribe(response=>{
          console.log("consoling");
          console.log(response);
         if(response)
         {
           this.handleGetResponse(response);
         }
      });
  }
  loadCourses()
  {
    this.courseService.getCourse(this.departmentId).subscribe(
      courses=>{
        console.log("under course response");
        console.log(courses);
        this.courses=courses;
      }
      

    );

  }
  selectedModules=[];
  selectedRequisite=[]
  handleGetResponse(response)
  {
    console.log("specfic course data");
    console.log(response);
    this.selectedModules[0]=response.module;
    this.selectedRequisite=response.prerequisite;
    
    this.form={
      code:response.course.code,
      title:response.course.title,
      lectureHours:response.course.lectureHours,
      laboratoryHours:response.course.laboratoryHours,
      requisite:response.prerequisite,
      creditHours:response.course.creditHours,
      ects:response.course.ECTS,
      module:response.module

  }
  this.form.module=this.selectedModules;
  this.form.requisite=this.selectedRequisite;

  console.log("consoling form");
  console.log(response.module);
  console.log(this.form.module);

  console.log("consoling requisite");
  console.log(this.form.requisite);

  }

}