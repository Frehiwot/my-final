import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {ManageCourse } from './manage-course';
import {ManageModule} from '../manage-course-module/module';

@Injectable({
  providedIn: 'root'
})
export class ManageCourseService {

  constructor(private httpClient:HttpClient) { }
  getCourse(departmentId):Observable<ManageCourse[]>{
    return this.httpClient.get<ManageCourse[]>(`http://localhost:8000/departmentHead/${departmentId}/courses`);
    
  }
  getSpecficCourse(courseCode):Observable<ManageCourse>{
    return this.httpClient.get<ManageCourse>(`http://localhost:8000/departmentHead/courses/${courseCode}`);
    
  }
  getModule(departmentId):Observable<ManageModule[]>{
    return this.httpClient.get<ManageModule[]>(`http://localhost:8000/departmentHead/${departmentId}/modules`);
    
  }
  addCourse(course,departmentId):Observable<ManageCourse>{
    return this.httpClient.post<ManageCourse>(`http://localhost:8000/departmentHead/${departmentId}/storeCourse/`,course);
  }
  deleteCourse(id,departmentId):Observable<{}>{
    return this.httpClient.request('delete',`http://localhost:8000/departmentHead/${departmentId}/course/${id}/delete`);
  }
  updateCourse(course,departmentId):Observable<ManageCourse>{
    console.log(departmentId);
    return this.httpClient.put<ManageCourse>(`http://localhost:8000/departmentHead/${departmentId}/course/edit/${course.code}`,course);
    
  }
}
