import { Component, OnInit } from '@angular/core';
import { ManageCourse } from './manage-course';
import {ManageCourseService} from './manage-course.service';
import {Router} from '@angular/router';
import { TokenService } from '../services/token.service';
import {ManageModule} from '../manage-course-module/module';
import Swal from 'sweetalert2/dist/sweetalert2.all.min.js'
import { NotifierService } from 'angular-notifier';
@Component({
  selector: 'app-manage-course',
  templateUrl: './manage-course.component.html',
  styleUrls: ['./manage-course.component.css']
})
export class ManageCourseComponent implements OnInit {

  courses: ManageCourse[];
  modules:ManageModule[];
  editCourse=null;
  addButton=null;
  listButton=null;
  departmentId=null;
  error=null;
  message=null;
  //selected=[];
  public form={
    code:null,
    title:null,
    lectureHours:null,
    laboratoryHours:null,
    requisite:[],
    creditHours:null,
    ects:null,
    module:null
  };
  total=0;
page=1;
pageSize=5
  expanded=[];
  constructor(private courseService: ManageCourseService,private Token: TokenService,private router: Router,private notification: NotifierService) { }

  ngOnInit() {
    this.router.navigateByUrl('/departmentHead/courses');
    this.departmentId=this.Token.getUser();
    console.log(this.departmentId);
    this.loadCourses();
    this.total=this.courses.length;
    this.loadModules();
   }
  loadCourses()
  {
    this.courseService.getCourse(this.departmentId).subscribe(
      course=>{
        console.log("under course response");
        console.log(course);
        this.handleGetResponse(course)
      }
      

    );

  }
  pageChanged(event){
    this.page = event;
  }
  loadModules()
  {
    this.courseService.getModule(this.departmentId).subscribe(
      module=>{
        this.modules=module;
        console.log(this.modules);
      }
    );

  }
  onSubmit(){
    console.log(this.form);
   
    this.editCourse=undefined
   this.courseService.addCourse(this.form,this.departmentId).subscribe(
     course=>this.handleResponse(course)
   );
  }
  handleGetResponse(course)
  {
    console.log(course);
    console.log("hello world");
    this.courses=course;
    console.log(this.courses);
    console.log(this.courses.length);
    for(var i:number=0;i<this.courses.length;i++)
      {
        this.expanded[i]=false;
  
      }
  }
  add(course: ManageCourse):void{
    console.log(course);
    this.editCourse=undefined
   this.courseService.addCourse(course,this.departmentId).subscribe(
     course=>this.handleResponse(course)

   );
  
  }
  handleResponse(course)
  {
    if(course.message === "succesfull")
    {
      this.notification.notify("success", " You have saved succesfully"); 
      this.courses.push(course.course);
    }
    else{
        this.handleError(course.error);
    }
  }
  handleError(error)
  {
    console.log(error);
    this.error=error;
    window.alert(this.error);
  }
  delete(course:ManageCourse):void{
    let responsee=null;
    Swal.fire({
      confirmButtonColor: '#2A66A7',
      title: "Are u sure u want to delete",


      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: "Ok",
      cancelButtonText: "cancel"
    }).then((result) => {
      if (result.value) {
        this.courseService.deleteCourse(course.code,this.departmentId).subscribe(response=>{
          console.log("under deleting");
          console.log(response);
          responsee=response;
          if(responsee.message)
          {
            this.notification.notify("success", " Deletion Succesfully"); 
            this.loadCourses();
           
          }
          else if(responsee.error)
          {
            this.notification.notify("warning", responsee.error);
          }
 
         
        });

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })
 
  }
  show(){
    console.log("add button clicked");
    if(!this.addButton)
    {
        console.log("addButton is null");
        this.addButton="show";
        
    }
    else{
      this.addButton=null;
    }
  }
  listButtons(){
    console.log("Actions Button  clicked");
    if(!this.listButton)
    {
        console.log("listButton is null");
        this.listButton="show";
        
    }
    else{
      this.listButton=null;
    }
  }
  toggleFloat(i:number)
  {
    console.log("under toggle");
    console.log(this.expanded[i]);
    this.expanded[i]=!this.expanded[i];
    console.log(this.expanded[i]);
  }
 
  edit(course)
  {
    this.editCourse=course;
  }
  updateCourse(course:ManageCourse):void{
    if(this.editCourse)
    {
      this.courseService.updateCourse(this.editCourse,this.departmentId).subscribe(Course => {
        const ix = Course ? this.courses.findIndex(h => h.id == Course.id) : -1
        if(ix > -1){
          this.courses[ix]=Course
        }
      })
      this.editCourse = undefined
    }
  }

}
