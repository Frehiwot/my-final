import { Component, OnInit } from '@angular/core';
import { InstructorStudentsService} from '../instructor-students.service';
import { Student } from '../student';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
import { NotifierService } from 'angular-notifier';
@Component({
  selector: 'app-instructor-students',
  templateUrl: './instructor-students.component.html',
  styleUrls: ['./instructor-students.component.css']
})
export class InstructorStudentsComponent implements OnInit {
   students:Student[];
   courseId:number
   editStudent:Student
   result:number
   studentResult=null
   instructorId:number
   studentsLength=0;
  constructor(private studentService: InstructorStudentsService,private router:Router,private route:ActivatedRoute,private notification: NotifierService) { }

  ngOnInit() {
    this.result=0;
    console.log("ng on init");
    this.route.paramMap.subscribe(
      params=>{
        this.courseId=+params.get('id');
        this.instructorId=+params.get('instructorId');
        console.log(this.courseId)

      }
    );
    this.studentService.getStudent(this.courseId,this.instructorId).subscribe(students=>this.handleResponse(students));
    
  }
  handleResponse(students)
  {
    console.log("getting");
    
    this.students=students;
    this.studentsLength=students.length;
    console.log(this.students);
  }

  edit(Student)
{
  console.log("in edit");
    this.editStudent=Student;
    // this.addButton="show";
    // this.updateBtn="update";

}

update(id: number): void{
  console.log("under update");
  console.log(this.result);
  console.log('student id');
  console.log(id);
  let studentt=null;
    
      this.studentService.updateGrade(this.courseId,id,this.result).subscribe(Student => {
       studentt=Student;
        if(studentt.message)
        {
         
          const ix = Student ? this.students.findIndex(h => h.id == Student.id) : -1
          if(ix > -1){
            this.students[ix]=studentt.student;
          }
          this.notification.notify("success", "Result Saved Succesfully");

        }
        else if(studentt.error){
          this.notification.notify("warning", studentt.errror);

        }
      
       
        console.log(Student)
      })
      
   
    //this.studentService.updateGrade(this.courseId,id,this.result).subscribe(Students=>this.studentResult=Students);
}


}
