import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student} from './student';

@Injectable({
  providedIn: 'root'
})
export class InstructorStudentsService {

  constructor(private httpClient:HttpClient) { }

  getStudent(courseId,instructorId):Observable<Student[]>{
    return this.httpClient.get<Student[]>(`http://localhost:8000/course/${courseId}/students`);
  }
  updateGrade(courseId,studentId,result):Observable<Student>
  {
    return this.httpClient.put<Student>(`http://localhost:8000/student/${studentId}/course/${courseId}/result`,result);
  }
}
