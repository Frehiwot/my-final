import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Course } from './course';
@Injectable({
  providedIn: 'root'
})
export class InstructorServiceService {

  constructor(private httpClient: HttpClient) { }

  public getCourses(instructorId):Observable<Course[]>
  {
     return this.httpClient.get<Course[]>(`http://localhost:8000/instructor/courses/${instructorId}`);
  }
}
