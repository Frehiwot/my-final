import { Component, OnInit } from '@angular/core';
import {InstructorServiceService } from '../instructor-service.service';
import { Course } from '../course';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
@Component({
  selector: 'app-instructor-page1',
  templateUrl: './instructor-page1.component.html',
  styleUrls: ['./instructor-page1.component.css']
})
export class InstructorPage1Component implements OnInit {
  courses:Course[];
   instructorId:number;
   coursesLength=0;
  constructor(private instructorService: InstructorServiceService,private router:Router,private route:ActivatedRoute) { }
   
  ngOnInit() {
    console.log("ng on init");
  this.route.paramMap.subscribe(
    params=>{
      this.instructorId=+params.get('id');
      console.log(this.instructorId)

    }
  );
  this.instructorService.getCourses(this.instructorId).subscribe(courses=>
    {
       this.courses=courses
       this.coursesLength=this.courses.length;
  }
    );
  }

}
