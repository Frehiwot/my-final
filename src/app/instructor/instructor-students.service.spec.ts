import { TestBed } from '@angular/core/testing';

import { InstructorStudentsService } from './instructor-students.service';

describe('InstructorStudentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstructorStudentsService = TestBed.get(InstructorStudentsService);
    expect(service).toBeTruthy();
  });
});
