import { Component, OnInit } from '@angular/core';
import { StudentCourseService} from './student-course.service';
import { Course } from './course';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-student-course',
  templateUrl: './student-course.component.html',
  styleUrls: ['./student-course.component.css']
})
export class StudentCourseComponent implements OnInit {
  courses:any=[]
  coursesLength:number
  optionalCourses:any=[]
  optionalCoursesLength:number
  registeredCourses:any=[]
  unRegisteredCourses:any=[]
  show=null;
  show2="showing";
  showUnregistered=null;
  message=null
  message2=null;
  message3=null;
  selectedStudent:number;
  courseForm: FormGroup;
  ECTS=0;
  creditHours=0;
  constructor(private httpClient:HttpClient,private fb:FormBuilder,private courseService: StudentCourseService,private router:Router,private route:ActivatedRoute) {
    this.courseForm=this.fb.group({
     course:this.fb.array([])
    });
   }

  ngOnInit() {
    
    this.route.paramMap.subscribe(
      params=>{
       this.selectedStudent=+params.get('id');
       console.log(this.selectedStudent)

      }
    );
    // this.httpClient.get(`http://127.0.0.1:7900/?baz=${this.selectedStudent}`).subscribe((response)=>{
    //  console.log("inside response");
    //  console.log(response);

    // });
    this.courseService.getCourses(this.selectedStudent).subscribe(course=>this.handleGetResponse(course));
  }
 onChange(course,isChecked)
 {
   const control=<FormArray>this.courseForm.controls.course;
   if(isChecked)
   {
     control.push(new FormControl(course));
   }
   else{
     const index=control.controls.findIndex(x => x.value === course);
     control.removeAt(index);
   }
 }
 onSubmit(courseForm)
 {
   console.log(courseForm.value.course);
   this.httpClient.post(`http://localhost:8000/student/${this.selectedStudent}/courses`,courseForm.value.course).subscribe(course=>this.handleResponse(course));
  
 }
 handleResponse(course)
 {
   console.log("hello world");
   console.log(course);
   
    if(course.cmd === "u have succesfully registered")
    {
        this.registeredCourses=course.registeredCourses;
        console.log(this.registeredCourses);
        this.message2="you have sucessfully registered for the following courses";
        this.show="showing";
        this.show2=null
    }
    else if(course.cmd === "semester load error"){
      this.message="u have exceeded the semester load,please select only the courses u need";
      this.courses=course.courses;
      this.coursesLength=this.courses.length
      // this.optionalCourses=course.optionalSemesterCourses;
      // console.log(this.optionalCourses);
      this.optionalCoursesLength=0;
      console.log(this.optionalCoursesLength);
      this.show2="showing";
      this.show=null;
    }
    else if(course.cmd === "not succesfull"){
      if(course.registeredCourses.length>0)
      {
        this.registeredCourses=course.registeredCourses;
        this.message2="you have sucessfully registered for the following courses";
        this.show="showing";
        this.show2=null;
       

      }
      if(course.unregisteredCourses.length>0){
        console.log("consoling insode");
        this.showUnregistered="showing";
        this.show2=null;
        this.unRegisteredCourses=course.unregisteredCourses;
        this.message3="You didnot full fill the pre-requisite of this course";
        

      }
      
     
    }
    
 }
 handleGetResponse(course)
 {

   console.log("in get"); 
   console.log(course);
   console.log(course.message);
   if(course.message === "already registered" )
   {
      this.show="showing";
      this.show2=null;
      this.registeredCourses=course.courses;
    
      if(this.registeredCourses.length > 0)
      {

        this.courses=course.courses;
        for(let i=0;i<this.courses.length;i++)
        {
           this.ECTS+=this.courses[i].ECTS;
           this.creditHours+=this.courses[i].creditHours;
        }
        this.message2="you have sucessfully registered for the following courses";
      
      }
      

   }
   else
   {
     if(course.message === "not registered")
     {
      this.courses=course.courses;
      this.coursesLength=this.courses.length
      this.optionalCourses=course.optionalSemesterCourses;
      console.log(this.optionalCourses);
      this.optionalCoursesLength=this.optionalCourses.length;
      console.log(this.optionalCoursesLength);
      for(let i=0;i<this.courses.length;i++)
      {
         this.ECTS+=this.courses[i].ECTS;
         this.creditHours+=this.courses[i].creditHours;
      }
      this.show2="showing";
       this.show=null;

        
     }
     else
    {
          this.message=course.message;
     }

     
   }
 }

}
