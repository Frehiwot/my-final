import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Course} from './course';

@Injectable({
  providedIn: 'root'
})
export class StudentCourseService {

  constructor(private httpClient: HttpClient) { }
  getCourses(studentId):Observable<Course[]>
  {
    return this.httpClient.get<Course[]>(`http://localhost:8000/student/${studentId}/courses`);
  }
}
