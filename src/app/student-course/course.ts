export class Course{
	id:number;
	code:string;
	title:string;
	module:string;
}